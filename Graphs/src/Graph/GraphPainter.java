package Graph;

import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class GraphPainter extends Group {
    private final double radius = 20.0;
    private final double distance = 50.0;

    public GraphPainter(){
        super();
    }
    public void drawGraph(OrientedGraph orientedGraph, Individual individual){
        LinkedList<Circle> circles = new LinkedList<>();
        LinkedList<Text> texts = new LinkedList<>();
        HashMap<String, Double> verticesPositionsX = new HashMap<>();
        HashMap<String, Double> verticesPositionsY = new HashMap<>();
        for(int i = 0; i < orientedGraph.getListOfVertexKeys().size(); i++){
            double posX = distance * (individual.getGeneCode().get(i) % individual.getNumberOfGenesInIndividual());
            double posY = distance * (individual.getGeneCode().get(i) / individual.getNumberOfGenesInIndividual());

            List keys = orientedGraph.getListOfVertexKeys();

            Text text = new Text(keys.get(i).toString());
            text.setFont(Font.font(null, FontWeight.BOLD, 14));
            text.setFill(Color.RED);
            text.setX(posX);
            text.setY(posY);

            texts.add(text);
            circles.add(new Circle(posX, posY, radius, Color.BLUE));
            verticesPositionsX.put(keys.get(i).toString(), posX);
            verticesPositionsY.put(keys.get(i).toString(), posY);
        }

        LinkedList<Line> lines = new LinkedList<>();
        List<Edge> edges = orientedGraph.getListOfEdges();
        for(Edge e: edges){
            String start = e.getStartingVertex().toString();
            String finish = e.getFinishVertex().toString();

            Line line = new Line();
            for(HashMap.Entry<String, Double> posX: verticesPositionsX.entrySet()){
                if (start.equals(posX.getKey())){
                    line.setStartX(posX.getValue());
                    break;
                }
            }
            for(HashMap.Entry<String, Double> posY: verticesPositionsY.entrySet()){
                if (start.equals(posY.getKey())){
                    line.setStartY(posY.getValue());
                    break;
                }
            }
            for(HashMap.Entry<String, Double> posX: verticesPositionsX.entrySet()){
                if (finish.equals(posX.getKey())){
                    line.setEndX(posX.getValue());
                    break;
                }
            }
            for(HashMap.Entry<String, Double> posY: verticesPositionsY.entrySet()){
                if (finish.equals(posY.getKey())){
                    line.setEndY(posY.getValue());
                    break;
                }
            }
            line.setStrokeWidth(1.75);
            line.setStroke(Color.GREEN);
            lines.add(line);
        }

        int n = orientedGraph.getListOfVertexKeys().size();
        for (int i = 0; i <= n; i++){
            Line line1 = new Line(0.0, distance * i, distance * n, distance * i);
            Line line2 = new Line(distance * i, 0.0 * i, distance * i, distance * n);
            line1.setStroke(Color.RED);
            line2.setStroke(Color.RED);
            this.getChildren().addAll(line1, line2);
        }

        this.getChildren().addAll(lines);
        this.getChildren().addAll(circles);
        this.getChildren().addAll(texts);
    }

    public double getVertexRadius(){
        return radius;
    }
    public double getDistanceBetweenVertices(){
        return distance;
    }

    /*public void showConnectivityComponents(List<List<? extends Comparable>> components){
        for(List<? extends Comparable> list: components){
            final Random random = new Random();
            Color color = Color.color(random.nextDouble(), random.nextDouble(), random.nextDouble());
            for(Comparable v : list){
                circles.get(Integer.parseInt(v.toString()) - 1).setFill(color);
            }
        }
    }
    public void showStrongConnectivityComponents(List<List<? extends Comparable>> components){
        for(List<? extends Comparable> list: components){
            final Random random = new Random();
            Color color = Color.color(random.nextDouble(), random.nextDouble(), random.nextDouble());
            for(Comparable v : list){
                circles.get(Integer.parseInt(v.toString()) - 1).setFill(color);
            }
        }
    }*/
}