package Graph;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.Random;

public class Pair {
    private Individual firstParent;
    private Individual secondParent;
    private int hemmingDistance;

    Pair(Individual firstParent, Individual secondParent){
        this.firstParent = firstParent;
        this.secondParent = secondParent;

    }

    public Individual getChild(){
        Individual individual = new Individual();
        LinkedList<Integer> geneCode = new LinkedList<>();

        Random random = new Random();
        int separator = random.nextInt(firstParent.getNumberOfGenesInIndividual());

        for(int i = 0; i < firstParent.getNumberOfGenesInIndividual(); i++){
            if(firstParent.getGeneCode().get(i) != secondParent.getGeneCode().get(i) && i <= separator){
                geneCode.add(firstParent.getGeneCode().get(i));
            }
            else if (firstParent.getGeneCode().get(i) != secondParent.getGeneCode().get(i) && i >= separator){
                geneCode.add(secondParent.getGeneCode().get(i));
            }
            else if (firstParent.getGeneCode().get(i) == secondParent.getGeneCode().get(i)){
                int gene;
                do {
                    gene = random.nextInt(firstParent.getNumberOfGenesInIndividual() * firstParent.getNumberOfGenesInIndividual());
                } while(gene == firstParent.getGeneCode().get(i));
                geneCode.add(gene);
            }
        }
        individual.setGeneCode(geneCode);
        return individual;
    }
    private void findHemmingDistance(){
        hemmingDistance = 0;
        for(int i = 0; i < firstParent.getGeneCode().size(); i++){
            if(!firstParent.getGeneCode().get(i).equals(secondParent.getGeneCode().get(i))){
                hemmingDistance++;
            }
        }
    }
    public int getHemmingDistance(){
        return hemmingDistance;
    }

    public final Comparator<Pair> HEMMING_DISTANCE = new Comparator<Pair>() {
        @Override
        public int compare(Pair first, Pair second) {
            return first.getHemmingDistance() - second.getHemmingDistance();
        }
    };
}