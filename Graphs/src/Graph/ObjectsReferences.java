package Graph;

import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class ObjectsReferences {
    private Stage primaryStage;
    private Scene scene;
    private BorderPane borderPane;
    private OrientedGraph orientedGraph;
    private GraphPainter graphPainter;

    public ObjectsReferences(){}

    public void setPrimaryStage(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }
    public void setScene(Scene scene){
        this.scene = scene;
    }
    public void setBorderPane(BorderPane borderPane){
        this.borderPane = borderPane;
    }
    public void setOrientedGraph(OrientedGraph orientedGraph){
        this.orientedGraph = orientedGraph;
    }
    public void setGraphPainter(GraphPainter graphPainter){
        this.graphPainter = graphPainter;
    }

    public Stage getPrimaryStage(){
        return primaryStage;
    }
    public Scene getScene(){
        return scene;
    }
    public BorderPane getBorderPane(){
        return borderPane;
    }
    public OrientedGraph getOrientedGraph(){
        return orientedGraph;
    }
    public GraphPainter getGraphPainter(){
        return graphPainter;
    }
}
