package Graph;

import java.util.LinkedList;
import java.util.Random;
import java.util.Set;

//Особь -- вариант укладки графа на плоскости
public class Individual {
    private int numberOfGenesInIndividual;
    private LinkedList<Integer> geneCode;
    private int suitability;
    private int lineIntersections;
    private int circleIntersection;

    Individual(){}
    Individual(Integer numberOfGenesInIndividual, Set<Integer> genes){
        this.numberOfGenesInIndividual = numberOfGenesInIndividual;

        geneCode = new LinkedList<>();
        LinkedList<Integer> setOfPossibleGenes = new LinkedList<>(genes);
        Random random = new Random();
        while(geneCode.size() < numberOfGenesInIndividual){
            int gene = setOfPossibleGenes.get(random.nextInt(setOfPossibleGenes.size()));
            if (!geneCode.contains(gene)) {
                geneCode.add(gene);
            }
        }
    }

    public void setGeneCode(LinkedList<Integer> geneCode){
        this.geneCode = geneCode;
        this.numberOfGenesInIndividual = geneCode.size();
    }
    public void setSuitability(int suitability){
        this.suitability = suitability;
    }
    public void setLinesIntersections(int lineIntersections){
        this.lineIntersections = lineIntersections;
    }
    public void setCircleIntersection(int circleIntersection){
        this.circleIntersection = circleIntersection;
    }

    public LinkedList<Integer> getGeneCode(){
        return geneCode;
    }
    public int getNumberOfGenesInIndividual(){
        return numberOfGenesInIndividual;
    }
    public int getSuitability(){
        return suitability;
    }
    public int getLinesIntersections(){
        return lineIntersections;
    }
    public int getCircleIntersection(){
        return circleIntersection;
    }
}