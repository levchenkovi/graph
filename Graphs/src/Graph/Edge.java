package Graph;

class Edge{
    private String startingVertex;
    private String finishVertex;
    private Integer weight;

    Edge(){}
    Edge(String start, String finish, int weight){
        startingVertex = start;
        finishVertex = finish;
        this.weight = weight;
    }

    public String getStartingVertex() {
        return startingVertex;
    }
    public String getFinishVertex() {
        return finishVertex;
    }
    public Integer getWeight(){
        return weight;
    }
}