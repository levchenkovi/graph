package Graph;

public class Point{
    double x;
    double y;

    public Point(double posX, double posY){
        this.x = posX;
        this.y = posY;
    }
    public boolean locatedBetweenPoints(Point p1, Point p2){
        if (p1.x == p2.x && x == p1.x && Math.min(p1.y, p2.y) < y && y < Math.max(p1.y, p2.y)){
            return true;
        }
        else if (p1.y == p2.y && y == p1.y && Math.min(p1.x, p2.x) < x && x < Math.max(p1.x, p2.x)){
            return true;
        }
        else if (Math.min(p1.x, p2.x) < x && x < Math.max(p1.x, p2.x) &&  Math.min(p1.y, p2.y) < y && y < Math.max(p1.y, p2.y)){
            return true;
        }
        return false;
    }
    public boolean equal(Point point){
        if (this.x == point.x && this.y == point.y){
            return true;
        }
        return false;
    }
}