package Graph;

import javafx.application.Application;
import javafx.scene.*;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import java.io.IOException;

public class Main extends Application {
    private static final int WIDTH = 900;
    private static final int HEIGHT = 600;

    public static void main(String[] args) throws IOException {
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        ObjectsReferences objectsReferences = new ObjectsReferences();
        objectsReferences.setPrimaryStage(primaryStage);
        BorderPane borderPane = new BorderPane();
        objectsReferences.setBorderPane(borderPane);
        Scene scene = new Scene(borderPane, WIDTH, HEIGHT);
        objectsReferences.setScene(scene);
        MyMenu menu = new MyMenu(objectsReferences);
        GraphPainter graphPainter = new GraphPainter();
        objectsReferences.setGraphPainter(graphPainter);
        borderPane.setTop(menu);
        borderPane.setCenter(graphPainter);

        primaryStage.setScene(scene);
        primaryStage.setTitle("Graph");
        primaryStage.show();
    }
}