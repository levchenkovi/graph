package Graph;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class OrientedGraph{
    private List<String> vertices;
    private List<Edge> edges;
    private HashMap<String, LinkedList<String>> adjacencyLists;
    private Population population;
    private ObjectsReferences objectsReferences;

    public OrientedGraph(){
        vertices = new LinkedList<>();
        edges = new LinkedList<>();;
        adjacencyLists = new HashMap<>();
    }
    public OrientedGraph(File file, ObjectsReferences objectsReferences){
        this.objectsReferences = objectsReferences;
        DotReader dotReader = new DotReader(file);

        vertices = dotReader.getVerticesList();
        edges = dotReader.getEdgesList();
        adjacencyLists = dotReader.getAdjacencyLists();

        CheckTheCorrectnessOfTheGraph();
    }
    private void CheckTheCorrectnessOfTheGraph(){
        TreeSet<String> vertices = new TreeSet<String>();
        try {
            for (HashMap.Entry<String, LinkedList<String>> list : adjacencyLists.entrySet()) {
                vertices.add(list.getKey());
            }
            for (HashMap.Entry<String, LinkedList<String>> lists : adjacencyLists.entrySet()) {
                LinkedList<String> list = lists.getValue();
                for (String v : list) {
                    if (!vertices.contains(v)) {
                        throw new IOException("Vertex " + v + " not exist");
                    }
                }
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void addVertex(String key){
        if (!vertices.contains(key)) {
            vertices.add(key);
            adjacencyLists.put(key, new LinkedList<String>());
        }
    }
    public void addEdge(String startingVertex, String finishVertex, Integer weight){
        edges.add(new Edge(startingVertex, finishVertex, weight));
    }
    public void addAdjacentVertex(String key, String v) {
        if (adjacencyLists.containsKey(key)) {
            adjacencyLists.get(key).add(v);
        }
        else{
            vertices.add(key);
            adjacencyLists.put(key, new LinkedList<String>());
            adjacencyLists.get(key).add(v);
        }
    }
    public void addAdjacencyList(String key, List<String> adjacencyVertices){
        if(adjacencyLists.containsKey(key)){
            for(String v : adjacencyVertices){
                adjacencyLists.get(key).add(v);
                if(!vertices.contains(v)){
                    vertices.add(v);
                }
            }
        }
        else{
            vertices.add(key);
            adjacencyLists.put(key, new LinkedList<String>());
            for(String v : adjacencyVertices){
                adjacencyLists.get(key).add(v);
                if(!vertices.contains(v)){
                    vertices.add(v);
                }
            }
        }
    }

    private void depthFirstSearch(String key_of_vertex, HashMap<String, Boolean> used, LinkedList<String> component){
        used.put(key_of_vertex, true);
        component.add(key_of_vertex);
        if(adjacencyLists.containsKey(key_of_vertex)){
            for(String v : adjacencyLists.get(key_of_vertex)){
                if(!used.get(v)){
                    depthFirstSearch(v, used, component);
                }
            }
        }
    }
    private void getOrder(String key_of_vertex, HashMap<String, Boolean> used, List<String> order){
        used.put(key_of_vertex, true);
        for(String v : adjacencyLists.get(key_of_vertex)){
            if(!used.get(v)){
                getOrder(v, used, order);
            }
        }
        order.add(key_of_vertex);
    }

    public List<String> findPathBetweenVertices(String s, String f){
        LinkedList<String> path = new LinkedList<>();

        if(!adjacencyLists.containsKey(s) || !adjacencyLists.containsKey(s)){
            return path;
        }

        LinkedList<String> q = new LinkedList<>();
        q.push(s);
        HashMap<String, Boolean> used = new HashMap<>();
        HashMap<String, String> predecessor = new HashMap<>();
        HashMap<String, Integer> distance = new HashMap<>();
        for(HashMap.Entry<String, LinkedList<String>> list : adjacencyLists.entrySet()){
            used.put(list.getKey(), false);
            predecessor.put(list.getKey(), null);
            distance.put(list.getKey(), -1);
        }

        used.put(s, true);
        distance.put(s, 0);
        while(!q.isEmpty()){
            String current = q.getFirst();
            q.removeFirst();
            adjacencyLists.get(current).size();
            for(String v : adjacencyLists.get(current)){
                if(!used.get(v)){
                    used.put(v, true);
                    q.push(v);
                    distance.put(v, distance.get(current) + 1);
                    predecessor.put(v, current);
                }
            }
        }

        if(used.get(f) != null){
            path.add(f);
            f = predecessor.get(f);
            while(f != null){
                path.add(f);
                f = predecessor.get(f);
            }
        }
        LinkedList<String> reversPath = new LinkedList<>();
        for(String v : path){
            reversPath.addFirst(v);
        }
        return reversPath;
    }
    public List<List<String>> findConnectivityComponents(){
        LinkedList<LinkedList<String>> components = new LinkedList<>();
        HashMap<String, Boolean> used = new HashMap<>();
        for(HashMap.Entry<String, LinkedList<String>> list: adjacencyLists.entrySet()){
            used.put(list.getKey(), false);
            for(String v : list.getValue()){
                used.put(v, false);
            }
        }

        for(HashMap.Entry<String, LinkedList<String>> list: adjacencyLists.entrySet()){
            if(!used.get(list.getKey())){
                components.add(new LinkedList<>());
                depthFirstSearch(list.getKey(), used, components.getLast());
            }
        }
        return (List)components;
    }
    public List findStrongConnectivityComponents(){
        LinkedList<LinkedList<String>> components = new LinkedList<>();

        LinkedList<String> order = new LinkedList<String>();
        HashMap<String, Boolean> used = new HashMap<>();
        for(HashMap.Entry<String, LinkedList<String>> v: adjacencyLists.entrySet()){
            used.put(v.getKey(), false);
        }

        for(HashMap.Entry<String, LinkedList<String>> v: adjacencyLists.entrySet()){
            if(!used.get(v.getKey())){
                getOrder(v.getKey(), used, order);
            }
        }

        for(HashMap.Entry<String, LinkedList<String>> v: adjacencyLists.entrySet()){
            used.put(v.getKey(), false);
        }

        OrientedGraph tGraph = getTransposedGraph();
        for(int i = 0; i < order.size(); i++){
            String key_of_vertex = order.get(order.size() - 1 - i);
            if(!used.get(key_of_vertex)){
                components.add(new LinkedList<String>());
                tGraph.depthFirstSearch(key_of_vertex, used, components.getLast());
            }
        }
        return (List)components;
    }

    public void createPopulation(){
        population = new Population();
        population.createPopulation(this);
    }
    public void evolution(){
        for(int i = 0; i < 10; i++){
            population.selectParents();
            population.createOffspring();
            population.checkSuitability();
        }
    }

    private OrientedGraph getTransposedGraph(){
        OrientedGraph graph = new OrientedGraph();
        TreeSet<String> keys = new TreeSet<String>();
        for(HashMap.Entry<String, LinkedList<String>> lists: adjacencyLists.entrySet()){
            keys.add(lists.getKey());
        }
        for(HashMap.Entry<String, LinkedList<String>> list: adjacencyLists.entrySet()){
            String key = list.getKey();
            for(String v : list.getValue()){
                if(keys.contains(key)){
                    graph.addAdjacentVertex(v, key);
                }
            }
        }
        return graph;
    }
    public Population getPopulation(){
        return population;
    }
    public List<String> getListOfVertexKeys(){
        //Collections.sort(vertices); /*???*/
        return vertices;
    }
    public List<Edge> getListOfEdges(){
        return edges;
    }
    public double getRadiusOfVertex(){
        return objectsReferences.getGraphPainter().getVertexRadius();
    }
    public double getDistanceBetweenVertices(){
        return objectsReferences.getGraphPainter().getDistanceBetweenVertices();
    }

    public void printThePathBetweenVertices(String s, String f){
        List<String> path = findPathBetweenVertices(s, f);
        for(int i = 0; i < path.size(); i++){
            System.out.print(path.get(i));
            if(i < path.size() - 1){
                System.out.print(" -> ");
            }
        }
        System.out.println();
    }
    public void printConnectivityComponents(){
        List<List<String>> components = findConnectivityComponents();

        int number = 0;
        for(List<String> comp : components){
            number++;
            if (number <= 10){
                switch (number){
                    case 1: System.out.print(number + "-st"); break;
                    case 2: System.out.print(number + "-nd"); break;
                    case 3: System.out.print(number + "-rd"); break;
                    default: System.out.print(number + "-th"); break;
                }
            }
            else if (number <= 20){
                System.out.print(number + "-th");
            }
            else{
                String str = new String();
                str = Integer.toString(number);
                int n = str.charAt(str.length() - 1) - '0';
                n += 10 * (str.charAt(str.length() - 1) - '0');
                if (10 < number && number <= 20){
                    System.out.print(number + "-th");
                }
                else{
                    switch (str.charAt(str.length() - 1)){
                        case 1: System.out.print(number + "-st"); break;
                        case 2: System.out.print(number + "-nd"); break;
                        case 3: System.out.print(number + "-rd"); break;
                        default: System.out.print(number + "-th"); break;
                    }
                }
            }
            System.out.print(" connectivity component: ");
            for(String i : comp){
                System.out.print(i + " ");
            }
            System.out.println();
        }
    }
    public void printStrongConnectivityComponents(){
        List<List<String>> components = findStrongConnectivityComponents();

        Integer number = 0;
        for(List<String> comp : components){
            number++;
            if (number <= 10){
                switch (number){
                    case 1: System.out.print(number + "-st"); break;
                    case 2: System.out.print(number + "-nd"); break;
                    case 3: System.out.print(number + "-rd"); break;
                    default: System.out.print(number + "-th"); break;
                }
            }
            else if (10 < number && number <= 20){
                System.out.print(number + "-th");
            }
            else{
                String str = new String();
                str = number.toString();
                int n = str.charAt(str.length() - 1) - '0';
                n += 10 * (str.charAt(str.length() - 1) - '0');
                if (10 < number && number <= 20){
                    System.out.print(number + "-th");
                }
                else{
                    switch (str.charAt(str.length() - 1)){
                        case 1: System.out.print(number + "-st"); break;
                        case 2: System.out.print(number + "-nd"); break;
                        case 3: System.out.print(number + "-rd"); break;
                        default: System.out.print(number + "-th"); break;
                    }
                }
            }
            System.out.print(" strong connectivity component: ");
            for(String i : comp){
                System.out.print(i + " ");
            }
            System.out.println();
        }
    }
}