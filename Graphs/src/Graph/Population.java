package Graph;

import java.util.*;

public class Population{
    private HashMap<String, Integer> correspondenceLists;
    private LinkedList<Individual> individuals;
    private LinkedList<Pair> pairs;
    private Set<Integer> setOfPossibleGenes;
    private OrientedGraph orientedGraph;
    private int numberOfGenesInIndividuals;

    public Population(){}
    public Population(OrientedGraph orientedGraph){
        pairs = new LinkedList<>();
        individuals = new LinkedList<>();
        this.orientedGraph = orientedGraph;
        createCorrespondence(orientedGraph.getListOfVertexKeys());
        createAllGenes();
    }
    public void createPopulation(OrientedGraph orientedGraph){
        pairs = new LinkedList<>();
        individuals = new LinkedList<>();
        this.orientedGraph = orientedGraph;
        createCorrespondence(orientedGraph.getListOfVertexKeys());
        createAllGenes();
    }
    private void createCorrespondence(List<String> listOfVertexKeys){
        //Устанавливаем соответствие между вершинами графа и локусами(позициями гена в храмасоме)
        correspondenceLists = new HashMap<>();
        int number = 0;
        for(String v : listOfVertexKeys){
            correspondenceLists.put(v, number);
            number++;
        }
        numberOfGenesInIndividuals = listOfVertexKeys.size();
    }
    private void createAllGenes(){
        setOfPossibleGenes = new TreeSet<Integer>();
        for(int i = 0; i < numberOfGenesInIndividuals * numberOfGenesInIndividuals; i++){
            setOfPossibleGenes.add(i);
        }
    }
    public void createRandomIndividual(){
        individuals.add(new Individual(numberOfGenesInIndividuals, setOfPossibleGenes));
    }
    public Individual getFirstIndividual(){
        return individuals.getFirst();
    }
    public HashMap<String, Integer> getCorrespondenceLists(){
        return correspondenceLists;
    }

    public void createGeneration(){
        int numberOfIndividuals = numberOfGenesInIndividuals * numberOfGenesInIndividuals;
        for(int i = 0; i < numberOfIndividuals; i++){
            createRandomIndividual();
        }
    }
    public void selectParents(){
        //Берём лучшую половину укладок графа
        individuals.sort(SUITABILITY);
        LinkedList<Individual> parents = new LinkedList<>();
        for(int i = 0; i < individuals.size() / 2; i++){
            parents.add(individuals.get(i));
        }
        individuals = parents;
    }
    public void createOffspring(){
        individuals.sort(SUITABILITY);
        pairs.clear();
        for(int i = 0; i < individuals.size() / 2; i++){
            pairs.add(new Pair(individuals.get(i), individuals.get(individuals.size() - 1 - i)));
        }
        for(Pair p : pairs){
            individuals.add(p.getChild());
            individuals.add(p.getChild());
        }
    }
    public Individual getBestIndividual(){
        individuals.sort(SUITABILITY);

        System.out.println("Best : " + individuals.getFirst().getSuitability());
        System.out.println("Lines : " + individuals.getFirst().getLinesIntersections());
        System.out.println("Circles : " + individuals.getFirst().getCircleIntersection());

        return individuals.getFirst();
    }

    public void checkSuitability(){
        for(Individual i : individuals){
            suitability(i);
        }
        individuals.sort(SUITABILITY);


        //Вывод
        for(int i = 0; i < individuals.size(); i++){
            if (i % 10 == 0){
                System.out.println();
            }
            System.out.print("|" + i + ": " + individuals.get(i).getSuitability() + "| ");
        }
        System.out.println();
        for(int i = 0; i < 5 + individuals.size(); i++ ){
            System.out.print("-");
        }
        System.out.println();
    }
    public void suitability(Individual individual){

        LinkedList<Point> vertexPosition = new LinkedList<>();
        LinkedList<Integer> geneCode = individual.getGeneCode();
        Integer numberOfGenes = geneCode.size();
        for(Integer g: geneCode){
            vertexPosition.add(new Point(g % numberOfGenes, g / numberOfGenes));
        }

        //Поиск уникальных рёбер, без учёта направления
        LinkedList<Edge> uniqueEdges = new LinkedList<>();
        for(Edge e: orientedGraph.getListOfEdges()){
            boolean unique = true;
            for(Edge ue: uniqueEdges){
                if (e.getStartingVertex().equals(ue.getStartingVertex()) && e.getFinishVertex().equals(ue.getFinishVertex()) ||
                        e.getStartingVertex().equals(ue.getFinishVertex()) && e.getFinishVertex().equals(ue.getStartingVertex())){
                    unique = false;
                    break;
                }
            }
            if(unique){
                uniqueEdges.add(e);
            }
        }

        //Ищем пересечения среди всех пар рёбер
        int linesIntersections = 0;
        for(int i = 0; i < uniqueEdges.size(); i++){
            for(int j = i + 1; j < uniqueEdges.size(); j++) {
                int numFirstS = correspondenceLists.get(uniqueEdges.get(i).getStartingVertex());
                int numFirstF = correspondenceLists.get(uniqueEdges.get(i).getFinishVertex());
                int numSecondS = correspondenceLists.get(uniqueEdges.get(j).getStartingVertex());
                int numSecondF = correspondenceLists.get(uniqueEdges.get(j).getFinishVertex());

                Point begin1 = vertexPosition.get(numFirstS);
                Point end1 = vertexPosition.get(numFirstF);
                Point begin2 = vertexPosition.get(numSecondS);
                Point end2 = vertexPosition.get(numSecondF);

                Point intersection = findIntersection(begin1, end1, begin2, end2);
                if (intersection.locatedBetweenPoints(begin1, end1) && intersection.locatedBetweenPoints(begin2, end2)){
                    linesIntersections++;

//                    System.out.print("Intersection between " + uniqueEdges.get(i).getStartingVertex() + "->" + uniqueEdges.get(i).getFinishVertex() + " ");
//                    System.out.println("and " + uniqueEdges.get(j).getStartingVertex() + "->" + uniqueEdges.get(j).getFinishVertex() + " edges:");
//                    System.out.print("(" + begin1.x + ", " + begin1.y + ") -> (" + end1.x + ", " + end1.y + ") and ");
//                    System.out.println("(" + begin2.x + ", " + begin2.y + ") -> (" + end2.x + ", " + end2.y + ")");
//                    System.out.println("x = : " + intersection.x);
//                    System.out.println("y = : " + intersection.y);
                }
            }
        }

        //Ищем пересечения среди всех пар рёбер и вершин
        int circleIntersections = 0;
        for(int i = 0; i < uniqueEdges.size(); i++) {

            int firstCircle = correspondenceLists.get(uniqueEdges.get(i).getStartingVertex());
            int secondCircle = correspondenceLists.get(uniqueEdges.get(i).getFinishVertex());

            for (int j = 0; j < uniqueEdges.size(); j++) {
                if (i != j) {
                    int startOfSegment = correspondenceLists.get(uniqueEdges.get(j).getStartingVertex());
                    int endOfSegment = correspondenceLists.get(uniqueEdges.get(j).getFinishVertex());

                    LinkedList<Point> points = new LinkedList<>();

                    Point begin = vertexPosition.get(startOfSegment);
                    Point end = vertexPosition.get(endOfSegment);
                    Point firstCenter = vertexPosition.get(firstCircle);
                    Point secondCenter = vertexPosition.get(secondCircle);

                    points.addAll(findIntersectionWithCircle(begin, end, firstCenter, orientedGraph.getRadiusOfVertex() / orientedGraph.getDistanceBetweenVertices()));
                    points.addAll(findIntersectionWithCircle(begin, end, secondCenter, orientedGraph.getRadiusOfVertex() / orientedGraph.getDistanceBetweenVertices()));
                    for (Point p : points) {
                        if (p.locatedBetweenPoints(begin, end) && !begin.equal(firstCenter) && !begin.equal(secondCenter) && !end.equal(firstCenter) && !end.equal(secondCenter)) {
                            circleIntersections++;
                        }
                    }
                }
            }
        }

        //Находим расстояния от начала сетки до всех вершин и определяем значение "неотсортированности"
        LinkedList<Double> distances = new LinkedList<>();
        for (Point point : vertexPosition) {
            double distance = Math.sqrt(point.x * point.x + point.y * point.y);
            distances.add(distance);
        }
        int unsorted = 0;
        for (int i = 0; i < distances.size(); i++){
            boolean next = false;
            for (int j = i+ 1; j < distances.size(); j++){
                if(distances.get(i) > distances.get(j) && !next){
                    unsorted++;
                    next = true;
                }
            }
        }

        individual.setLinesIntersections(linesIntersections);
        individual.setCircleIntersection(circleIntersections);
        individual.setSuitability(linesIntersections + circleIntersections + unsorted);
    }
    public Point findIntersection(Point begin1, Point end1, Point begin2, Point end2){
        double A1 = end1.y - begin1.y;
        double B1 = begin1.x - end1.x;
        double C1 = begin1.y * (end1.x - begin1.x) - begin1.x * (end1.y -begin1.y);

        double A2 = end2.y - begin2.y;
        double B2 = begin2.x - end2.x;
        double C2 = begin2.y * (end2.x - begin2.x) - begin2.x * (end2.y -begin2.y);

        double x = -1.0;
        double y = -1.0;
        if (A1 != 0 && A2 != 0 && B1 != 0 && B2 != 0){
            x = ((B1 * C2) / B2 - C1) / (A1 - (A2 * B1) / B2);
            y = (-A1 * x - C1) / B1;
        }
        else if(A1 == 0 && A2 != 0 && B1 != 0 && B2 != 0){
            x = (B2 * C1) / (A2 * B1) - (C2 / A2);
            y = -C1 / B1;
        }
        else if (A1 != 0 && A2 == 0 && B1 != 0 && B2 != 0){
            x = (B1 * C2) / (A1 * B2) - (C1 / A1);
            y = -C2 / B2;
        }
        else if (A1 != 0 && A2 != 0 && B1 == 0 && B2 != 0){
            y = (A2 * C1) / (B2 * A1) - (C2 / B2);
            x = -C1 / A1;
        }
        else if (A1 != 0 && A2 != 0 && B1 != 0 && B2 == 0){
            y = (A1 * C2) / (B1 * A2) - (C1 / B1);
            x = -C2 / A2;
        }
        else if (A1 == 0 && A2 != 0 && B1 != 0 && B2 == 0){
            x = (- C2) / A2;
            y = (- C1) / B1;
        }
        else if (A1 != 0 && A2 == 0 && B1 == 0 && B2 != 0){
            x = (- C1) / A1;
            y = (- C2) / B2;
        }

        return new Point(x, y);
    }
    public LinkedList<Point> findIntersectionWithCircle(Point begin, Point end, Point center, double R){
        LinkedList<Point> intersections = new LinkedList<>();

        double A1 = end.y - begin.y;;
        double B1 = begin.x - end.x;
        double C1 = begin.y * (end.x - begin.x) - begin.x * (end.y -begin.y);

        double A2 = -2 * center.x;
        double B2 = -2 * center.y;
        double C2 = center.x * center.x + center.y * center.y - R * R;

        if (B1 != 0) {
            double k = -A1 / B1;
            double b = -C1 / B1;

            double a1 = 1.0 + k * k;
            double b1 = 2.0 * k * b + A2 + B2 * k;
            double c1 = b * b + B2 * b + C2;

            double D = b1 * b1 - 4.0 * a1 * c1;
            if (D > 0.0){
                double X1 = (-b1 + Math.sqrt(D)) / (2.0 * a1);
                double Y1 = -A1 * X1 / B1 - C1 / B1;

                double X2 = (-b1 - Math.sqrt(D)) / (2.0 * a1);
                double Y2 = -A1 * X2 / B1 - C1 / B1;

                intersections.add(new Point(X1, Y1));
                intersections.add(new Point(X2, Y2));
            }
            else if (D == 0.0){
                double X1 = -b1 / (2.0 * a1);
                double Y1 = -A1 * X1 / B1 - C1 / B1;
                intersections.add(new Point(X1, Y1));
            }
        }
        else {
            double a1 = 1.0;
            double b1 = B2;
            double c1 = C1 * C1 / A1 * A1 - A2 * C1 / A1 + C2;

            double X1 = -C1 / A1;
            double D = b1 * b1 - 4.0 * a1 * c1;
            if (D > 0.0){
                double Y1 = (-b1 + Math.sqrt(D)) / (2.0 * a1);
                double Y2 = (-b1 - Math.sqrt(D)) / (2.0 * a1);

                intersections.add(new Point(X1, Y1));
                intersections.add(new Point(X1, Y2));
            }
            else if (D == 0.0){
                double Y1 = -A1 * X1 / B1 - C1 / B1;
                intersections.add(new Point(X1, Y1));
            }
        }
        return intersections;
    }

    public static final Comparator<Individual> SUITABILITY = new Comparator<Individual>() {
        @Override
        public int compare(Individual first, Individual second) {
            return first.getSuitability() - second.getSuitability();
        }
    };
}