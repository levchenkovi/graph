package Graph;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

class DotException extends Exception{
    DotException(){
        super();
    }
    DotException(String str){
        super(str);
    }
}

public class DotReader{
    enum Type{
        graph,
        digraph
    }

    private int ch;
    private StringBuilder graphName;
    private Type type;
    private List<String> vertices;
    private List<Edge> edges;
    private HashMap<String, LinkedList<String>> adjacencyLists;

    private int pos;
    private Integer numberOfVertices;
    private LinkedList<String> verticesKeys;
    private Boolean addedVertices;
    private FileReader fileReader;

    public DotReader(File fileName) {
        pos = 0;
        graphName = new StringBuilder("");
        vertices = new LinkedList<String>();
        edges = new LinkedList<Edge>();
        adjacencyLists = new HashMap<String, LinkedList<String>>();

        try {
            fileReader = new FileReader(fileName);
            ch = fileReader.read();
            readGraph();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (DotException e) {
            try {
                while (ch != '\n' && ch != '\r' && ch != -1) {
                    ch = fileReader.read();
                    System.out.print((char)ch);
                }
                System.out.println();
                for(int i = 0; i <= pos; i++){
                    System.out.print(' ');
                }
                System.out.println('^');
                while (ch != -1) {
                    ch = fileReader.read();
                    System.out.print((char)ch);
                }
                System.out.println();
                System.out.println(e);
                e.printStackTrace();
            }
            catch (IOException i){
                System.out.println(i);
            }
        }
        catch (IOException e){
            e.printStackTrace();
        }
    }

    private void readGraph() throws DotException, IOException {
        readTypeOfGraph();
        readGraphName();
        readLeftBrace();
        while (Character.isLetter(ch) || Character.isDigit(ch)){
            readConnectivity();
        }
        readRightBrace();
        CheckEndOfFile();
    }
    private void readTypeOfGraph() throws DotException, IOException {
        StringBuilder t = new StringBuilder(new String(""));
        while (Character.isLetter(ch)) {
            t.append((char) ch);
            next();
        }
        if(isSpace()){
            nextCh();
        }

        switch (String.valueOf(t)) {
            case "graph":
                type = Type.graph;
                break;
            case "digraph":
                type = Type.digraph;
                break;
            default: throw new DotException("Wrong type of graph!");
        }
    }
    private void readGraphName() throws DotException, IOException {
        while(Character.isLetter(ch) || Character.isDigit(ch) || ch == '_'){
            graphName.append((char)ch);
            next();
        }
        if(isSpace()) {
            nextCh();
        }
    }
    private void readLeftBrace() throws DotException, IOException {
        if (ch == '{') {
            nextCh();
        }
        else {
            throw new DotException("Left brace \'{\' not found");
        }
    }
    private void readConnectivity() throws DotException, IOException {
        verticesKeys = new LinkedList<String>();
        numberOfVertices = 0;
        addedVertices = false;

        boolean expectedVertexName = true;
        while(ch != -1) {
            if (Character.isLetter(ch) || Character.isDigit(ch) && expectedVertexName) {
                readVertexName();
                expectedVertexName = false;
            }
            else if (ch == '-'){
                readEdge();
                expectedVertexName = true;
            }
            else if (ch == '['){
                readAttribute();
                expectedVertexName = false;
            }
            else if (ch == ';'){
                nextCh();
                break;
            }
            else if (expectedVertexName) {
                throw new DotException("Expected vertex name");
            }
            else{
                throw new DotException("Expected ';' or '-'!");
            }
        }
        if (!addedVertices){
            addEdges("1");
        }
    }

    private void readVertexName() throws DotException, IOException {
        StringBuilder vName = new StringBuilder("");
        while(Character.isLetter(ch) || Character.isDigit(ch)){
            vName.append((char)ch);
            next();
        }
        if(isSpace()){
            nextCh();
        }

        if(numberOfVertices == 0 && adjacencyLists.containsKey(vName.toString())){
            verticesKeys.add(vName.toString());
        }
        else if(numberOfVertices == 0 && !adjacencyLists.containsKey(vName.toString())) {
            verticesKeys.add(vName.toString());
            adjacencyLists.put(vName.toString(), new LinkedList<String>());
            vertices.add(vName.toString());
        }
        else if(numberOfVertices != 0 && adjacencyLists.containsKey(vName.toString())){
            LinkedList<String> list = (LinkedList<String>) adjacencyLists.get(verticesKeys.getLast());
            list.add(vName.toString());
            verticesKeys.add(vName.toString());
        }
        else if(numberOfVertices != 0 && !adjacencyLists.containsKey(vName.toString())){
            LinkedList<String>  list = (LinkedList<String>) adjacencyLists.get(verticesKeys.getLast());
            list.add(vName.toString());
            adjacencyLists.put(vName.toString(), new LinkedList<String>());
            verticesKeys.add(vName.toString());
            vertices.add(vName.toString());
        }
        numberOfVertices++;
    }
    private void readEdge() throws DotException, IOException {
        next();
        if((type == Type.graph && ch == '-') || (type == Type.digraph && ch == '>')){
            nextCh();
        }
        else{
            throw new DotException("Incorrect edge indication");
        }
    }

    private void readAttribute() throws DotException, IOException{
        StringBuilder attributeName = new StringBuilder("");
        StringBuilder attribute = new StringBuilder("");

        nextCh();
        readAttributeName(attributeName);
        readEqual();

        switch (attributeName.toString()){
            case "weight" :
                readIntegerAttribute(attribute);
                addEdges(attribute.toString());
                break;
            default:
                throw new DotException("Wrong attribute!");
        }
        if(ch == ']'){
            nextCh();
        }
        else{
            throw new DotException("Expected ']'!");
        }
    }
    private void readAttributeName(StringBuilder attributeName) throws DotException, IOException {
        if(Character.isLetter(ch) || Character.isDigit(ch)){
            while(Character.isLetter(ch) || Character.isDigit(ch)){
                attributeName.append((char)ch);
                nextCh();
            }
        }
        else{
            throw new DotException("Expected attribute name!");
        }
    }
    private void readEqual() throws DotException, IOException {
        if(ch == '='){
            nextCh();
        }
        else{
            throw new DotException("Expected attribute name!");
        }
    }
    private void readIntegerAttribute(StringBuilder attribute) throws DotException, IOException {
        if(Character.isDigit(ch)){
            while(Character.isDigit(ch)){
                attribute.append((char)ch);
                nextCh();
            }
        }
        else {
            throw new DotException("Expected '\"' ");
        }
    }
    private void addEdges(String attribute){
        if (numberOfVertices > 1) {
            for (int i = 1; i < verticesKeys.size(); i++) {
                edges.add(new Edge(verticesKeys.get(i - 1), verticesKeys.get(i), Integer.parseInt(attribute)));
            }
        }
        addedVertices = true;
    }

    private void readRightBrace() throws DotException, IOException {
        if (ch == '}') {
            nextCh();
        }
        else {
            throw new DotException("Expected brace '}'!");
        }
    }
    private void CheckEndOfFile() throws DotException, IOException{
        while(ch != -1){
            next();
            if(!isSpace()){
                throw new DotException("Expected end of file!");
            }
        }
    }

    private void nextCh() throws DotException, IOException {
        if (ch == -1) {
            throw new DotException("End of file reached!");
        }
        ch = fileReader.read();
        //System.out.print((char)ch);
        if(ch == '\t'){
            pos += 3;
        }
        pos++;
        if (isSpace()){
            while (ch != -1 && isSpace()) {
                ch = fileReader.read();
                //System.out.print((char)ch);
                if(ch == '\t'){
                    pos += 3;
                }
                if(ch == '\n' ||  ch == '\r'){
                    pos = 0;
                }
            }
        }
    }
    private void next() throws DotException, IOException {
        if (ch == -1) {
            throw new DotException("End of file reached!");
        }
        else{
            ch = fileReader.read();
            //System.out.print((char)ch);
            if(ch == '\t'){
                pos += 3;
            }
            pos++;
            if(ch == '\n' ||  ch == '\r'){
                pos = 0;
            }
        }
    }
    private boolean isSpace(){
        return ch == 9 || ch == 10 || ch == 13 || ch == 32;
    }

    public String getGraphName(){
        return graphName.toString();
    }
    public List<String> getVerticesList(){
        return vertices;
    }
    public List<Edge> getEdgesList(){
        return edges;
    }
    public HashMap<String, LinkedList<String>> getAdjacencyLists(){
        return adjacencyLists;
    }
}