package Graph;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.input.KeyCombination;
import javafx.stage.FileChooser;

import java.io.File;
import java.util.List;

class MyMenu extends MenuBar {
    private Menu fileMenu;
    private Menu editMenu;
    private MenuItem openFileItem;
    private MenuItem exitItem;
    private MenuItem redrawGraph;
    private MenuItem showCCItem;
    private MenuItem showSCCItem;
    private FileChooser fileChooser;

    MyMenu(ObjectsReferences objectsReferences){
        super();

        fileMenu = new Menu("File");
        openFileItem = new MenuItem("Open File");
        exitItem = new MenuItem("Exit");

        editMenu = new Menu("Edit");
        showCCItem = new MenuItem("Show connectivity components");
        showSCCItem = new MenuItem("Show strong connectivity components");
        redrawGraph = new MenuItem("Redraw the graph");

        this.getMenus().add(fileMenu);
        fileMenu.getItems().addAll(openFileItem, exitItem);

        this.getMenus().add(editMenu);
        editMenu.getItems().addAll(showCCItem, showSCCItem, redrawGraph);

        setOnAction(objectsReferences);
    }
    private void setOnAction(ObjectsReferences objectsReferences){
        openFileItem.setAccelerator(KeyCombination.keyCombination("Ctrl+O"));
        exitItem.setAccelerator(KeyCombination.keyCombination("Ctrl+X"));

        openFileItem.setOnAction( new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                fileChooser = new FileChooser();
                List<File> files = fileChooser.showOpenMultipleDialog(objectsReferences.getPrimaryStage());
                OrientedGraph orientedGraph = new OrientedGraph(files.get(0), objectsReferences);
                objectsReferences.setOrientedGraph(orientedGraph);

                orientedGraph.createPopulation();
                orientedGraph.getPopulation().createGeneration();
                orientedGraph.getPopulation().checkSuitability();
                orientedGraph.evolution();

                objectsReferences.getGraphPainter().getChildren().clear();
                objectsReferences.getGraphPainter().drawGraph(orientedGraph, orientedGraph.getPopulation().getBestIndividual());
            }
        });
        exitItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                System.exit(0);
            }
        });
        showSCCItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
            }
        });
        showCCItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
            }
        });
        redrawGraph.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                objectsReferences.getGraphPainter().getChildren().clear();
                OrientedGraph orientedGraph = objectsReferences.getOrientedGraph();
                orientedGraph.createPopulation();
                orientedGraph.getPopulation().createGeneration();
                orientedGraph.getPopulation().checkSuitability();
                orientedGraph.evolution();
                objectsReferences.getGraphPainter().drawGraph(orientedGraph, orientedGraph.getPopulation().getBestIndividual());
            }
        });
    }
}