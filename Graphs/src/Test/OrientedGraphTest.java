package Test;

import Graph.ObjectsReferences;
import Graph.OrientedGraph;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class OrientedGraphTest {

    private void exampleOfGraph_one(OrientedGraph graph){
        graph.addAdjacencyList("1", Arrays.asList("2", "3"));
        graph.addAdjacencyList("2", Arrays.asList("1", "5"));
        graph.addAdjacencyList("3", Arrays.asList("4", "5", "6"));
        graph.addAdjacencyList("4", Arrays.asList("3", "5", "6"));
        graph.addAdjacencyList("5", Arrays.asList("3", "4", "6", "7"));
        graph.addAdjacencyList("6", Arrays.asList("3", "4", "5"));
        graph.addAdjacencyList("7", Arrays.asList("8", "9"));
        graph.addAdjacencyList("8", Arrays.asList("7", "9"));
        graph.addAdjacencyList("9", Arrays.asList("7", "8"));
    }
    private void exampleOfGraph_two(OrientedGraph graph){
        graph.addAdjacencyList("1", Arrays.asList("2"));
        graph.addAdjacencyList("3", Arrays.asList("4"));
    }

    @Test
    public void getListOfVertexKeys_test1() {
        OrientedGraph graph = new OrientedGraph();
        List<String> list = graph.getListOfVertexKeys();

        List<String> myList = new LinkedList<>();
        Assert.assertEquals(list, myList);
    }
    @Test
    public void getListOfVertexKeys_test2() {
        OrientedGraph graph = new OrientedGraph();
        graph.addVertex("1");
        graph.addVertex("2");
        graph.addVertex("3");
        graph.addVertex("4");
        List<String> list = graph.getListOfVertexKeys();

        List<String> myList = new LinkedList<>();
        myList.add("1");
        myList.add("2");
        myList.add("3");
        myList.add("4");

        Assert.assertEquals(list, myList);
    }

    @Test
    public void findPathBetweenVertices_test1() {
        OrientedGraph graph = new OrientedGraph();

        List<String> list = graph.findPathBetweenVertices("1", "1");
        List<String> myList = new LinkedList<>();

        Assert.assertEquals(list, myList);
    }
    @Test
    public void findAnyPathBetweenVertices_test2() {
        OrientedGraph graph = new OrientedGraph();
        exampleOfGraph_one(graph);

        List<String> list = graph.findPathBetweenVertices("1", "1");
        List<String> myList = new LinkedList<>();
        myList.add("1");
        Assert.assertEquals(list, myList);

        list = graph.findPathBetweenVertices("1", "5");
        myList.clear();
        myList.add("1");
        myList.add("3");
        myList.add("5");
        Assert.assertEquals(list, myList);

        list = graph.findPathBetweenVertices("1", "9");
        myList.clear();
        myList.add("1");
        myList.add("3");
        myList.add("5");
        myList.add("7");
        myList.add("9");
        Assert.assertEquals(list, myList);
    }

    @Test
    public void findConnectivityComponents_test1() {
        OrientedGraph graph = new OrientedGraph();
        List<List<String>> list = graph.findConnectivityComponents();
        List<List<String>> myList = new LinkedList<>();
        Assert.assertEquals(list, myList);
    }
    @Test
    public void findConnectivityComponents_test2() {
        OrientedGraph graph = new OrientedGraph();

        exampleOfGraph_one(graph);
        List<List<String>> list = graph.findConnectivityComponents();
        List<List<String>> myList = new LinkedList<>();
        myList.add(Arrays.asList("1", "2", "5", "3", "4", "6", "7", "8", "9"));
        Assert.assertEquals(list, myList);

        graph = new OrientedGraph();
        exampleOfGraph_two(graph);
        list = graph.findConnectivityComponents();
        myList.clear();
        myList.add(Arrays.asList("1", "2"));
        myList.add(Arrays.asList("3", "4"));
        Assert.assertEquals(list, myList);
    }

    @Test
    public void findStrongConnectivityComponents_test1() {
        OrientedGraph graph = new OrientedGraph();

        exampleOfGraph_one(graph);
        List<List<String>> list = graph.findStrongConnectivityComponents();
        List<List<String>> myList = new LinkedList<>();
        myList.add(Arrays.asList("1", "2"));
        myList.add(Arrays.asList("5", "3", "4", "6"));
        myList.add(Arrays.asList("7", "8", "9"));
        Assert.assertEquals(list, myList);

        graph = new OrientedGraph();
        exampleOfGraph_two(graph);
        list = graph.findConnectivityComponents();
        myList.clear();
        myList.add(Arrays.asList("1", "2"));
        myList.add(Arrays.asList("3", "4"));
        Assert.assertEquals(list, myList);
    }

    @Test
    public void testOfPopulation(){
//        ObjectsReferences objectsReferences = new ObjectsReferences();
//
//        File fileName = new File("C:\\Users\\vladi\\IdeaProjects\\Graphs\\graph.dot");
//        OrientedGraph orientedGraph = new OrientedGraph(fileName, objectsReferences);
//        orientedGraph.createPopulation();
//        orientedGraph.getPopulation().createGeneration();
//        orientedGraph.getPopulation().checkSuitability();
//        orientedGraph.getPopulation().selectParents();
    }
}